<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

/**
 * Description of EventDetailsController
 *
 * @author student
 */
class ErrorController extends CController {
    private $_actionDirectory = 'application.controllers.error.';

    public function actions() {
        return array(
            'index' => $this->_actionDirectory.'IndexAction'
        );
    }
}
