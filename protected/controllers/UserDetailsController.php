<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

/**
 * Description of UserDetailsController
 *
 * @author student
 */
class UserDetailsController extends CController {
    private $_actionDirectory = 'application.controllers.userDetails.';

    public function actions() {
        return array(
            'index' => $this->_actionDirectory.'IndexAction'
        );
    }
}
