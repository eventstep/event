<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

/**
 * Description of HomeController
 *
 * @author student
 */
class HomeController extends CController {
    private $_actionDirectory = 'application.controllers.home.';

    public function actions() {
        return array(
            'index' => $this->_actionDirectory.'IndexAction',
            'updateContent' => $this->_actionDirectory.'UpdateContentAction'
        );
    }
}
