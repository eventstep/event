<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

/**
 * Description of IndexAction
 *
 * @author student
 */
class IndexAction extends CAction {
    public function run() {
        $this->controller->render('index');
    }
}
