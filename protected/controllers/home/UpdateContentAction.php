<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

/**
 * Description of IndexAction
 *
 * @author student
 */
class UpdateContentAction extends CAction {
    public function run() {
        $viewParams = new ViewParams();
        $viewParams->sidebar->isShow = FALSE;
        
        $this->controller->renderPartial('index', array('hello' => 'Hello again!!!'));
        Yii::app()->end();
    }
}
