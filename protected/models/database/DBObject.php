<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of DBObject
 *
 * @author snxGalaxy
 */
abstract class DBObject {

    public abstract function getId();
    
    /**
     *
     * @var ContentType
     */
    protected $contentType;
    
    /**
     * Applies new ban
     * @param Ban $ban
     */
    public static function setObjectBan($dbObject, $ban) {
        // Готовим ответ
        $modelResponse = new ModelResponse();
        
        // Присваеваем данные объекта бану
        $ban->id_content_type = $dbObject->contentType;
        $ban->id_content_key = $dbObject->getId();
        
        // Пробуем создать запись в БД
        try {
            $ban->create();
        } catch (Exception $ex) {
            // Сообщаем об ошибках
            $modelResponse->setIsCorrect(false);
            $modelResponse->setMessage(sprintf('Saving record error: %s', $ex));
        }
        
        // Возвращаем ответ
        return $modelResponse;
    }
    
    /**
     * Returns all ban records
     */
    public static function getObjectBan($dbObject) {
        // Готовим ответ
        $modelResponse = new ModelResponse();
        
        $banResponse = NULL;
        $banResult = NULL;
        $queryResult = NULL;
        
        // Делаем запрос к БД в поисках банов для объекта (получим ActiveRecord класс)
        $queryResult = BanActiveRecord::model()->find(
                array(
                    'select' => '*',
                    'condition' => 'id_content_type = :id_content_type AND id_content_key = :id_content_key AND DATE_ADD(date_start, INTERVAL Period SECOND) >= NOW()',
                    'params' => array(
                        ':id_content_type' => $dbObject->contentType,
                        ':id_content_key' => $dbObject->getId()
                    )
                ));
        
        // Если запрос имеет записи
        if ($queryResult !== NULL) {
            if (is_array($queryResult)) {
                $banResult = array();
                
                // Для каждого найденного бана
                foreach ($queryResult as $b) {
                    // Готовим контрактный бан
                    $banResponse = new BanContract();
                    // Заполняем все контрактные поля полученным ActiveRecord баном
                    $banResponse->assign($b);
                    
                    $banResult[] = $banResponse;
                }
            } else {
                $banResult = new BanContract();
                $banResult->assign($queryResult);
            }
        } else {
            // Если записей нет, добавляем сообщение, но не говорим что была ошибка ($modelResponse->setIsCorrect(FALSE);)
            $modelResponse->setMessage('No records in database');
        }
        
        // Добавляем найденные записи в ответ
        $modelResponse->setObject($banResult);
        
        return $modelResponse;
    }
    
    /**
     * Applies new comment
     * @param Comment $comment
     */
    public static function setObjectComment($dbObject, $comment) {}
    
    /**
     * Returns setted range of comments
     * @param datetime $from
     * @param int $count
     * @param datetime $to
     */
    public static function getObjectComment($dbObject, $from, $count, $to = NULL) {}
    
    /**
     * Sets keywords string
     * @param KeywordContract $keyword
     */
    public static function setObjectKeyword($dbObject, $keyword) {}
    
    /**
     * Returns all keywords
     */
    public static function getObjectKeyword($dbObject) {}
    
    /**
     * Applies new mark
     * @param Mark $mark
     */
    public static function setMark($dbObject, $mark) {}
    
    /**
     * Returns all marks
     */
    public static function getMark($dbObject) {}
    
    /**
     * Applies new media
     * @param Media $media
     */
    public static function setMedia($dbObject, $media) {}


    /**
     * Returns setted range of media
     * @param datetime $from
     * @param int $count
     * @param datetime $to
     */
    public static function getMedia($dbObject, $from, $count, $to = NULL) {}
    
    /**
     * Applies new subscriber
     * @param Subscriber $subscriber
     */
    public static function setSubscriber($dbObject, $subscriber) {}
    
    /**
     * Returns setted range of subscribers
     * @param datetime $from
     * @param int $count
     * @param datetime $to
     */
    public static function getSubscriber($dbObject, $from, $count, $to = NULL) {}
    
    /**
     * Inserts new object to database
     */
    public abstract function create();
    
    /**
     * Returns specific record from database
     * @param filter $from
     * @param int $count if = 0 all
     */
    public abstract function read($filterFrom, $count = 0);
    
    /**
     * Возращяет массив найденый Объектов из Базы Данных, по заданому фильтру
     * @param mixed $filter
     */
    public abstract function find($filter);
    
    /**
     * Updates record in database
     */
    public abstract function update();
    
    /**
     * Delete record from database
     */
    public abstract function delete();
    
    /**
     * Assign ActiveRecord class
     * @param mixed $obj ActiveRecord class
     */
    public abstract function assign($obj);
}
