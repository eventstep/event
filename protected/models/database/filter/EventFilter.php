<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of EventFilter
 *
 * @author student
 */
class EventFilter extends Filter {
    const ID = 0;
    const TITLE = 1;
    const DESCRIPTION = 2;
    const PRICE = 3;
    const CAPACITY = 4;
    const DATE_START = 5;
}
