<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of BanFilter
 *
 * @author student
 */
class SubscriberFilter extends Filter {
    const ID = 0;
    const ID_CONTENT_TYPE = 1;
    const ID_CONTENT_KEY = 2;
    const ID_USER = 3;
    const ID_SUBSCRIBER_ROLE = 4;
}
