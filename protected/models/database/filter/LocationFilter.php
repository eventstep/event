<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of BanFilter
 *
 * @author student
 */
class LocationFilter extends Filter {
    const ID = 0;
    const ID_COORDINATE = 1;
    const ID_CITY = 2;
    const ADDRESS = 3;
    const NAME = 4;
}
