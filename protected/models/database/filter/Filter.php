<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of Filter
 *
 * @author student
 */
class Filter {
    public $keyType;
    public $keyValue;
    
    public function __construct() {
        $this->keyType = 0;
    }
    
    /**
     * Групирует массив по keyType ключу.
     * Эта функция нужна для обработки методов `Find` у контракт классах
     * Контроллерам эта функция не нужна.
     * @author Gavrilyuc
     * @param mixed $ArrayCriteria Массив объектов Filter
     * @return mixed Возращяет массив
     */
    public static function LINQ_SelectFromArray($ArrayCriteria) {
        $arraySelect = array();
	for($i = 0; $i < count($ArrayCriteria); $i++) {
            $arraySelect[$i] = array();
            $arraySelect[$i][0] = $ArrayCriteria[$i];
            $tmp_index = 1;
            for($j = ($i + 1); $j < count($ArrayCriteria); $j++) {
                if($arraySelect[$i][0]->keyType == $ArrayCriteria[$j]->keyType) {
                    $arraySelect[$i][$tmp_index] = $ArrayCriteria[$j];
                    $tmp_index++;
		}
            }
        }
	for($i = 0; $i < count($arraySelect); $i++) {
            if($arraySelect == null) continue;
            for($j = ($i + 1); $j < count($arraySelect); $j++) {
                if($arraySelect[$j] == null) continue;
                if($arraySelect[$i][0]->keyType == $arraySelect[$j][0]->keyType) {
                    $arraySelect[$j] = null;
		}
            }
	}
	$rezult = array();
	$tmp_index = 0;
	for($i = 0; $i < count($arraySelect); $i++) {
            if($arraySelect[$i] == null) continue;
                $rezult[$tmp_index] = $arraySelect[$i];
		$tmp_index++;
        }
	return $rezult;
    }
}
