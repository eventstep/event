<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of BanFilter
 *
 * @author student
 */
class BanFilter extends Filter {
    const ID = 0;
    const ID_CONTENT_TYPE = 1;
    const ID_CONTENT_KEY = 2;
    const DATE_START = 3;
    const PERIOD = 4;
}
