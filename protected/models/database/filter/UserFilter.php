<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of BanFilter
 *
 * @author student
 */
class UserFilter extends Filter {
    const ID = 0;
    const FIRSTNAME = 1;
    const MIDDLENAME = 2;
    const LASTNAME = 3;
    const GENDER = 4;
    const EMAIL = 5;
    const PHONE = 6;
    const SKYPE = 7;
    const SITE = 8;
    const BIRTHDAY = 9;
    const ID_MEDIA = 10;
    const PASSWORD = 11;
    const ID_STATUS = 12;
    const IS_MODERATED = 13;
    const ID_USER_ROLE = 14;
}
