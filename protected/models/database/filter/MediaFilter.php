<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 26, 2014
 * Description: 
 */

/**
 * Description of BanFilter
 *
 * @author student
 */
class MediaFilter extends Filter {
    const ID = 0;
    const ID_CONTENT_TYPE = 1;
    const ID_CONTENT_KEY = 2;
    const ID_USER = 3;
    const URL = 4;
    const TITLE = 5;
}
