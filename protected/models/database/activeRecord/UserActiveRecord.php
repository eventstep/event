<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of BanActiveRecord
 *
 * This is the model class for table "Evt_User".
 *
 * The followings are the available columns in table 'Evt_User':
 * @property integer $Id
 * @property string $FirstName
 * @property string $MiddleName
 * @property string $LastName
 * @property integer $IdGender
 * @property string $Email
 * @property string $Phone
 * @property string $Skype
 * @property string $Site
 * @property integer $IdLocation
 * @property string $Birthday
 * @property integer $IdMedia
 * @property string $Password
 * @property integer $IdStatus
 * @property integer $IsModerated
 * @property integer $IdUserRole
 * @author snxGalaxy
 */
class UserActiveRecord extends CActiveRecord {
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }
    
    public function tableName() {
        return 'evt_user';
    }
}