<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of BanActiveRecord
 *
 * @author snxGalaxy
  * The followings are the available columns in table 'evt_event':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property double $price
 * @property integer $id_location
 * @property integer $id_event_type
 * @property integer $capacity
 * @property string $date_start
 */
class EventActiveRecord extends CActiveRecord {
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }
    
    public function tableName() {
        return 'evt_event';
    }
}
