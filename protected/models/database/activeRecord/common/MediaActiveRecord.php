<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of BanActiveRecord
 *
 * This is the model class for table "Evt_Media".
 *
 * The followings are the available columns in table 'Evt_Media':
 * @property integer $Id
 * @property integer $IdContentType
 * @property integer $IdContentKey
 * @property integer $IdUser
 * @property string $url
 * @property string $title
 * @author snxGalaxy
 */
class MediaActiveRecord extends CActiveRecord {
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }
    
    public function tableName() {
        return 'evt_media';
    }
}
