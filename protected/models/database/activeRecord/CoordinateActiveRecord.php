<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CoordinateActiveRecord
 *
 * @author snxGalaxy
 */
class CoordinateActiveRecord extends CActiveRecord {
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }
    
    public function tableName() {
        return 'evt_coordinate';
    }
}
