<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of BanActiveRecord
 *
 * @author snxGalaxy
 */
class LocationActiveRecord extends CActiveRecord {
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }
    
    public function tableName() {
        return 'evt_location';
    }
}
