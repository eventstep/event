<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class EventContract extends DBObject {
    private $_eventActiveRecord;
    
    public function getId() {
        return $this->_eventActiveRecord->id;
    }
    public function getTitle() {
        return $this->_eventActiveRecord->title;
    }
    public function setTitle($value) {
        $this->_eventActiveRecord->title = $value;
    }
    public function getDescription() {
        return $this->_eventActiveRecord->description;
    }
    public function setDescription($value) {
        $this->_eventActiveRecord->description = $value;
    }
    public function getPrice() {
        return $this->_eventActiveRecord->price;
    }
    public function setPrice($value) {
        $this->_eventActiveRecord->price = $value;
    }
    public function getLocation() {
        $locationFilter = new LocationFilter();
        $locationFilter->keyType = LocationFilter::ID;
        $locationFilter->keyValue = $this->_eventActiveRecord->id_location;
        return LocationContract::find($locationFilter)->getObject();
    }
    public function setLocation() {
        $this->_eventActiveRecord->id_location = $value->getId();
    }
    public function getEventType() {
        $eventTypeFilter = new EventTypeFilter();
        $eventTypeFilter->keyType = EventTypeFilter::ID;
        $eventTypeFilter->keyValue = $this->_eventActiveRecord->id_event_type;
        
        return EventTypeContract::find($eventTypeFilter)->getObject();
    }
    public function setEventType() {
        $this->_eventActiveRecord->id_event_type = $value->getId();
    }
    public function getCapacity() {
        return $this->_eventActiveRecord->capacity;
    }
    public function setCapacity($value) {
        $this->_eventActiveRecord->capacity = $value;
    }
    public function getDateStart() {
        return $this->_eventActiveRecord->date_start;
    }
    public function setDateStart($value) {
        $this->_eventActiveRecord->date_start = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::EVENT;
        $this->_eventActiveRecord = new EventActiveRecord();
    }

    public function create() {
        $this->_eventActiveRecord->model()->save();
    }
    public function delete() {
        $this->_eventActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array EventContract
     * @param mixed $filter Array EventFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_event` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = EventActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new EventContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{EventContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{EventContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof EventFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case EventFilter::CAPACITY:
                    $sql = $sql."`capacity`=".$criteria->keyValue." ".$tag;
                    break;
                case EventFilter::DATE_START:
                    $sql = $sql."`date_start` like '".$criteria->keyValue."' ".$tag;
                    break;
                case EventFilter::DESCRIPTION:
                    $sql = $sql."`date_start` like '".$criteria->keyValue."' ".$tag;
                    break;
                case EventFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case EventFilter::PRICE:
                    $sql = $sql."`price`=".$criteria->keyValue." ".$tag;
                    break;
                case EventFilter::TITLE:
                    $sql = $sql."`title` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    
    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
    public function update() {
        $this->_eventActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_eventActiveRecord = $obj;
    }
}
