<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class CoordinateContract extends DBObject {
    private $_coordinateActiveRecord;
    
    public function getId() {
        return $this->_coordinateActiveRecord->id;
    }
    public function getLatitude() {
        return $this->_coordinateActiveRecord->latitude;
    }
    public function setLatitude($value) {
        $this->_coordinateActiveRecord->latitude = $value;
    }
    public function getLongitude() {
        return $this->_coordinateActiveRecord->longitude;
    }
    public function setLongitude($value) {
        $this->_coordinateActiveRecord->longitude = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::COORDINATE;
        $this->_coordinateActiveRecord = new CoordinateActiveRecord();
    }

    public function create() {
        $this->_coordinateActiveRecord->model()->save();
    }
    public function delete() {
        $this->_coordinateActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array CoordinateContract
     * @param mixed $ArrayCriteria Array CoordinateFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_coordinate` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = CoordinateActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new CoordinateContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{CoordinateContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{CoordinateContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof CoordinateFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case CoordinateFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case CoordinateFilter::LATITUDE:
                    $sql = $sql."`latitude`=".$criteria->keyValue." ".$tag;
                    break;
                case CoordinateFilter::LONGITUDE:
                    $sql = $sql."`longitude`=".$criteria->keyValue." ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
    public function update() {
        $this->_coordinateActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_coordinateActiveRecord = $obj;
    }
}
