<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class SubscriberContract extends DBObject {
    private $_subscriberActiveRecord;

    public function getContentType() {
        return $this->_subscriberActiveRecord->id_content_type;
    }
    public function setContentType($value) {
        $this->_subscriberActiveRecord->id_content_type = $value;
    }
    public function getContentKey() {
        return $this->_subscriberActiveRecord->id_content_key;
    }
    public function setContentKey($value) {
        $this->_subscriberActiveRecord->id_content_key = $value;
    }
    public function getUser() {
        $userFilter = new UserFilter();
        $userFilter->keyType = UserFilter::ID;
        $userFilter->keyValue = $this->_subscriberActiveRecord->id_user;
        
        return UserContract::Find($userFilter)->getObject();
    }
    public function setUser($value) {
        $this->_subscriberActiveRecord->id_user = $value->id;
    }
    public function getSubscriberRole() {
        return $this->_subscriberActiveRecord->subscriber_role;
    }
    public function setSubscriberRole($value) {
        $this->_subscriberActiveRecord->subscriber_role = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::SUBSCRIBER;
        $this->_subscriberActiveRecord = new SubscriberActiveRecord();
    }

    public function create() {
        $this->_subscriberActiveRecord->model()->save();
    }
    public function delete() {
        $this->_subscriberActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array SubscriberContract
     * @param mixed $ArrayCriteria Array SubscriberFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_subscriber` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = SubscriberActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new SubscriberContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{SubscriberContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{SubscriberContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof SubscriberFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case SubscriberFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case SubscriberFilter::ID_CONTENT_KEY:
                    $sql = $sql."`id_content_key`=".$criteria->keyValue." ".$tag;
                    break;
                case SubscriberFilter::ID_CONTENT_TYPE:
                    $sql = $sql."`id_content_type`=".$criteria->keyValue." ".$tag;
                    break;
                case SubscriberFilter::ID_SUBSCRIBER_ROLE:
                    $sql = $sql."`id_subscriber_role`=".$criteria->keyValue." ".$tag;
                    break;
                case SubscriberFilter::ID_USER:
                    $sql = $sql."`id_user`=".$criteria->keyValue." ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
    public function update() {
        $this->_subscriberActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_subscriberActiveRecord = $obj;
    }
    public function getId() {
        return $this->_subscriberActiveRecord->id;
    }
}
