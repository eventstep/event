<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of BanContract
 *
 * @author snxGalaxy
 */
class BanContract extends DBObject {
    private $_banActiveRecord;
    
    public function getId() {
        return $this->_banActiveRecord->id;
    }
    public function getContentType() {
        return $this->_banActiveRecord->id_content_type;
    }
    public function setContentType($value) {
        $this->_banActiveRecord->id_content_type = $value;
    }
    public function getContentKey() {
        return $this->_banActiveRecord->id_content_key;
    }
    public function setContentKey($value) {
        $this->_banActiveRecord->id_content_key = $value;
    }
    public function getDatetime() {
        return $this->_banActiveRecord->date_start;
    }
    public function setDatetime($value) {
        $this->_banActiveRecord->date_start = $value;
    }
    public function getPeriod() {
        return $this->_banActiveRecord->period;
    }
    public function setPeriod($value) {
        $this->_banActiveRecord->period = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::BAN;
        $this->_banActiveRecord = new BanActiveRecord();
    }

    public function create() {
        $this->_banActiveRecord->model()->save();
    }
    public function delete() {
        $this->_banActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array MediaContract
     * @param mixed $ArrayCriteria Array MediaFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_ban` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = BanActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new BanContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{BanContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{BanContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof BanFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case BanFilter::DATE_START:
                    break;
                case BanFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case BanFilter::ID_CONTENT_KEY:
                    $sql = $sql."`id_content_key`=".$criteria->keyValue." ".$tag;
                    break;
                case BanFilter::ID_CONTENT_TYPE:
                    $sql = $sql."`id_content_type`=".$criteria->keyValue." ".$tag;
                    break;
                case BanFilter::ID_USER:
                    $sql = $sql."`id_user`=".$criteria->keyValue." ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }

    public function update() {
        $this->_banActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_banActiveRecord = $obj;
    }
}
