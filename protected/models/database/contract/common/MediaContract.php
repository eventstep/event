<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class MediaContract extends DBObject {
    
    private $_mediaActiveRecord;
    
    public function getId() {
        return $this->_mediaActiveRecord->Id;
    }
    public function setId($value) {
        $this->_mediaActiveRecord->id = $value;
    }
    public function getContentType() {
        return $this->_mediaActiveRecord->id_content_type;
    }
    public function setContentType($value) {
        $this->_mediaActiveRecord->id_content_type = $value;
    }
    public function getContentKey() {
        return $this->_mediaActiveRecord->id_content_key;
    }
    public function setContentKey($value) {
        $this->_mediaActiveRecord->id_content_key = $value;
    }
    public function getUrl() {
        return $this->_mediaActiveRecord->url;
    }
    public function setUrl($value) {
        $this->_mediaActiveRecord->url = $value;
    }
    public function getUser() {
        $userFilter = new UserFilter();
        $userFilter->keyType = UserFilter::ID;
        $userFilter->keyValue = $this->_mediaActiveRecord->id_user;
        
        return UserContract::Find($userFilter)->getObject();
    }
    public function setUser($value) {
        $this->_mediaActiveRecord->id_user = $value->id;
    }
    public function getTitle() {
        return $this->_mediaActiveRecord->title;
    }
    public function setTitle($value) {
        $this->_mediaActiveRecord->title = $value;
    }

    public function __construct() {
        $this->contentType = ContentType::MEDIA;
        $this->_mediaActiveRecord = new MediaActiveRecord();
    }
    
    public function create() {
        $this->_mediaActiveRecord->model()->save();
    }
    public function delete() {
        $this->_mediaActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array MediaContract
     * @param mixed $ArrayCriteria Array MediaFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_media` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = MediaActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new MediaContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{MediaContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{MediaContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof MediaFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case MediaFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case MediaFilter::ID_CONTENT_KEY:
                    $sql = $sql."`id_content_key`=".$criteria->keyValue." ".$tag;
                    break;
                case MediaFilter::ID_CONTENT_TYPE:
                    $sql = $sql."`id_content_type`=".$criteria->keyValue." ".$tag;
                    break;
                case MediaFilter::ID_USER:
                    $sql = $sql."`id_user`=".$criteria->keyValue." ".$tag;
                    break;
                case MediaFilter::TITLE:
                    $sql = $sql."`title` like'".$criteria->keyValue."' ".$tag;
                    break;
                case MediaFilter::URL:
                    $sql = $sql."`url` like'".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
    public function update() {
        $this->_mediaActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_mediaActiveRecord = $obj;
    }
}
