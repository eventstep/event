<?php
/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class CommentContract extends DBObject {
    private $_commentActiveRecord;
    
    public function getId() {
        return $this->_commentActiveRecord->id;
    }
    public function getContentType() {
        return $this->_commentActiveRecord->id_content_type;
    }
    public function setContentType($value) {
        $this->_commentActiveRecord->id_content_type = $value;
    }
    public function getContentKey() {
        return $this->_commentActiveRecord->id_content_key;
    }
    public function setContentKey($value) {
        $this->_commentActiveRecord->id_content_key = $value;
    }
    public function getUser() {
        $userFilter = new UserFilter();
        $userFilter->keyType = UserFilter::ID;
        $userFilter->keyValue = $this->_commentActiveRecord->id_user;
        return UserContract::Find($userFilter)->getObject();
    }
    public function setUser($value) {
        $this->_commentActiveRecord->id_user = $value->getId();
    }
    public function getText() {
        return $this->_commentActiveRecord->text;
    }
    public function setText($value) {
        $this->_commentActiveRecord->text = $value;
    }
    public function getDateTime() {
        return $this->_commentActiveRecord->date_time;
    }
    public function setDateTime($value) {
        $this->_commentActiveRecord->date_time = $value;
    }

    public function __construct() {
        $this->contentType = ContentTypeContract::COMMENT;
        $this->_commentActiveRecord = new CommentActiveRecord();
    }

    public function create() {
        $this->_commentActiveRecord->model()->save();
    }
    public function delete() {
        $this->_commentActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array MediaContract
     * @param mixed $ArrayCriteria Array MediaFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_comment` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = CommentActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new CommentContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{CommentContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{CommentContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof CommentFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case CommentFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case CommentFilter::ID_CONTENT_KEY:
                    $sql = $sql."`id_content_key`=".$criteria->keyValue." ".$tag;
                    break;
                case CommentFilter::ID_CONTENT_TYPE:
                    $sql = $sql."`id_content_type`=".$criteria->keyValue." ".$tag;
                    break;
                case CommentFilter::ID_USER:
                    $sql = $sql."`id_user`=".$criteria->keyValue." ".$tag;
                    break;
                case CommentFilter::TEXT:
                    $sql = $sql."`text` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }

    public function update() {
        $this->_commentActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_commentActiveRecord = $obj;
    }
}
