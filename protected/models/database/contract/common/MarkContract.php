<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class MarkContract extends DBObject {
    private $_markActiveRecord;
    
    public function getId() {
        return $this->_markActiveRecord->id;
    }
    public function getContentType() {
        return $this->_markActiveRecord->id_content_type;
    }
    public function setContentType($value) {
        $this->_markActiveRecord->id_content_type = $value;
    }
    public function getContentKey() {
        return $this->_markActiveRecord->id_content_key;
    }
    public function setContentKey($value) {
        $this->_markActiveRecord->id_content_key = $value;
    }
    public function getMark() {
        return $this->_markActiveRecord->mark;
    }
    public function setMark($value) {
        $this->_markActiveRecord->mark = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContact::MEDIA;
        $this->_markActiveRecord = new MarkActiveRecord();
    }

    public function create() {
        $this->_markActiveRecord->model()->save();
    }
    public function delete() {
        $this->_markActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array MediaContract
     * @param mixed $ArrayCriteria Array MediaFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_mark` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = MarkActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new MarkContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{MarkContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{MarkContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof MarkFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case MarkFilter::CONTENT_KEY:
                    $sql = $sql."`id_content_key`=".$criteria->keyValue." ".$tag;
                    break;
                case MarkFilter::CONTENT_TYPE:
                    $sql = $sql."`id_content_type`=".$criteria->keyValue." ".$tag;
                    break;
                case MarkFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case MarkFilter::MARK:
                    $sql = $sql."`mark`=".$criteria->keyValue." ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }

    public function update() {
        $this->_markActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_markActiveRecord = $obj;
    }
}