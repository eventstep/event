<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class CityContract extends DBObject {
    private $_cityActiveRecord;
    
    public function getId() {
        return $this->_cityActiveRecord->id;
    }
    public function getName() {
        return $this->_cityActiveRecord->name;
    }
    public function setName($value) {
        $this->_cityActiveRecord->name = $value;
    }
    public function getCountry() {
        $countryFilter = new CountryFilter();
        $countryFilter->keyType = CountryFilter::ID;
        $countryFilter->keyValue = $this->_cityActiveRecord->id_country;
        
        return CountryContract::find($countryFilter)->getObject();
    }
    public function setCountry($value) {
        $this->_cityActiveRecord->id_country = $value->id;
    }

    public function __construct() {
        $this->contentType = ContentTypeContract::CITY;
        $this->_cityActiveRecord = new CityActiveRecord();
    }

    public function create() {
        $this->_cityActiveRecord->model()->save();
    }
    public function delete() {
        $this->_cityActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array CityContract
     * @param mixed $ArrayCriteria Array CityFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_city` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = CityActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new CityContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{CityContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{CityContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof CityFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case CityFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case CityFilter::ID_COUNTRY:
                    $sql = $sql."`id_country`=".$criteria->keyValue." ".$tag;
                    break;
                case CityFilter::NAME:
                    $sql = $sql."`id` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }


    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }

    public function update() {
        $this->_cityActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_cityActiveRecord = $obj;
    }
}
