<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class EventTypeContract extends DBObject {
    private $_eventTypeActiveRecord;
    
    public function getName() {
        return $this->_eventTypeActiveRecord->name;
    }
    
    public function setName($value) {
        $this->_eventTypeActiveRecord->name = $value;
    }
    
    public function __construct() {
        $this->_eventTypeActiveRecord = new EventTypeActiveRecord();
    }

    public function create() {
        $this->_eventTypeActiveRecord->model()->save();
    }

    public function delete() {
        $this->_eventTypeActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array EventTypeContract
     * @param mixed $filter Array EventTypeFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_event_type` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = EventTypeActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new EventTypeContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{EventTypeContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{EventTypeContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof EventTypeFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case EventTypeFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case EventTypeFilter::NAME:
                    $sql = $sql."`name` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        
    }

    public function update() {
        $this->_eventTypeActiveRecord->model()->update();
    }

    public function assign($obj) {
        $this->_eventTypeActiveRecord = $obj;
    }

    public function getId() {
        return $this->_eventTypeActiveRecord->id;
    }
}
