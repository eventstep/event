<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy & Gavrilyuc
 */
class UserRoleContract {
    const USER = 1;
    const MEMBER = 2;
    const LEADER = 3;
    const MODERATOR = 4;
    const ADMINISTRATOR = 5;
}
