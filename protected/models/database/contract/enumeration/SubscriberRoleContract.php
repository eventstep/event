<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class SubscriberRoleContract {
    const SUBSCRIBER = 1;
    const MEMBER = 2;
    const LEADER = 3;
    const SPONSOR = 4;
}
