<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class ContentTypeContract {
    const USER = 1;
    const EVENT = 2;
    const LOCATION = 3;
    const COMMENT = 4;
    const MEDIA = 5;
    const CITY = 6;
    const COUNTRY = 7;
    const COORDINATE = 8;
    const MARK = 9;
    const KEYWORD = 10;
    const SUBSCRIBER = 11;
    const BAN = 12;
}
