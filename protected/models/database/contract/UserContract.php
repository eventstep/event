<?php
/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 * @author snxGalaxy
 */

/**
 * Description of User
 *
 * @author snxGalaxy & Gavrilyuc
 */
class UserContract extends DBObject {
    
    private $ThisEvtUser;
    
    public function getId() {
        return $this->ThisEvtUser->id;
    }
    public function getFirstName() {
        return $this->ThisEvtUser->first_name;
    }
    public function setFirstName($value) {
        $this->ThisEvtUser->first_name = $value;
    }
    public function getMiddleName() {
        return $this->ThisEvtUser->midle_name;
    }
    public function setMiddleName($value) {
        $this->ThisEvtUser->midle_name = $value;
    }
    public function getLastName() {
        return $this->ThisEvtUser->LastName;
    }
    public function setLastName($value) {
        $this->ThisEvtUser->LastName = $value;
    }
    public function getGender() {
        return $this->ThisEvtUser->id_gender;
    }
    public function setGender($value) {
        if($value == GenderContract::MALE || $value == GenderContract::FEMALE) {
            $this->ThisEvtUser->id_gender = $value;
        }
    }
    public function getEmail() {
        return $this->ThisEvtUser->email;
    }
    public function setEmail($value) {
        $this->ThisEvtUser->email = $value;
    }
    public function getPhone() {
        return $this->ThisEvtUser->phone;
    }
    public function setPhone($value) {
        $this->ThisEvtUser->phone = $value;
    }
    public function getSkype() {
        return $this->ThisEvtUser->skype;
    }
    public function setSkype($value) {
        $this->ThisEvtUser->skype = $value;
    }
    public function getSite() {
        return $this->ThisEvtUser->site;
    }
    public function setSite($value) {
        $this->ThisEvtUser->site = $value;
    }
    public function getBirthday() {
        return $this->ThisEvtUser->birthday;
    }
    public function setBirthday($value) {
        $this->ThisEvtUser->birthday = $value;
    }
    public function getPassword() {
        return $this->ThisEvtUser->password;
    }
    public function setPassword($value) {
        $this->ThisEvtUser->password = $value;
    }
    public function getStatus() {
        return $this->ThisEvtUser->id_status;
    }
    public function setStatus($value) {
        $this->ThisEvtUser->id_status = $value;
    }
    public function getRole() {
        return $this->ThisEvtUser->id_user_role;
    }
    public function setRole($value) {
        $this->ThisEvtUser->id_user_role = $value;
    }
    public function getIsModerated() {
        return $this->ThisEvtUser->is_moderated;
    }
    public function setIsModerated($value) {
        $this->ThisEvtUser->is_moderated = $value;
    }
    
    public function getImage() {
        $criteria = new MediaFilter();
        $criteria->keyType = MediaFilter::ID;
        $criteria->keyValue = $this->ThisEvtUser->id_media;
        $rr = MediaContract::find($criteria);
        $rr instanceof ModelResponse;
        return $rr->getObject();
    }
    public function setImage($value) {
        if($value instanceof MediaContract) {
            $this->ThisEvtUser->id_media = $value->getId();
        }
    }
    public function getLocation() {
        $criteria = new LocationFilter();
        $criteria->keyType = LocationFilter::ID;
        return LocationContract::find($criteria)->getObject();
    }
    public function setLocation($value) {
        if($value instanceof LocationContract) {
            $this->ThisEvtUser->id_location = $value->getId();
        }
    }

    public function __construct() {
        $this->contentType = ContentType::USER;
        $this->ThisEvtUser = new UserActiveRecord();
    }
    
    /* HELPER Methods */
    
    /**
     * Validate and register new user in database.
     * @param User $user
     */
    public static function Register($user) {
        $rezult = UserContract::validateUser($user);
        if($rezult->getIsCorrect() === TRUE) {
            if($user->ThisEvtUser->save()) {
                $rezult->setMessage($rezult->getMessage()."[Register Successful]");
            } else {
                $rezult->setMessage($rezult->getMessage()."[Register Error]");
                $rezult->setIsCorrect(FALSE);
            }
        }
        return $rezult;
    }
    private static function validateUser($user) {
        $rezult = new ModelResponse();
        $rezult->setObject(NULL);
        $rezult->setIsCorrect(FALSE);
        $rezult->setMessage("{UserContract::register} [param_user instanceof UserContract = false]");
        if($user instanceof UserContract) {
            $len = strlen($user->getPassword());
            $len_email = $user->getEmail();
            // Email Check
            if($len_email >= 6) {
                $checkEmail = EvtUser::model()->find('email=:email', [':email'=>$user->getEmail()]);
                if($checkEmail === NULL) {
                    if($len >= 4) {
                        $rezult->setMessage("{UserContract::register}[Email OK][Password OK]");
                        $rezult->setIsCorrect(TRUE);
                    } else {
                        $rezult->setMessage("{UserContract::register}[Email OK][Password length >= 4]");
                    }
                } else {
                    $rezult->setMessage("{UserContract::register}[Email is created database]");
                }
            } else {
                $rezult->setMessage("{UserContract::register}[Email length >= 6]");
            }
        }
        return $rezult;
    }

    /**
     * Validate and set user online
     * @param string $email
     * @param string $password
     */
    public static function Authentify($email, $password) {
        $method = "{UserContract::authentify} ";
        
        $rezult = new MediaContract();
        $rezult->setIsCorrect(FALSE);
        $rezult->setMessage($method."[not user]");
        $user = UserActiveRecord::model()->find('email=:email and password=:password', [':email'=>$email, ':password'=>$password]);
        if($user !== NULL) {
            // Set Object User
            $ctr_user = new UserContract();
            $ctr_user->ThisEvtUser = $user;
            
            // Set Return Object User
            $rezult->setObject($ctr_user);
            $rezult->setIsCorrect(TRUE);
            $rezult->setMessage($method."[Authentify Successful]");
        }
        return $rezult;
    }
    
    
    /* OVERIDE's Methods */
    
    /*
     * Сохраняет изменение данных в объекте UserContract
     */
    public function update() {
        $this->ThisEvtUser->save();
    }
    public function create() {
        $this->ThisEvtUser->save();
    }
    
    /*
     * Ищет пользователей по списку заданых критерий
     * @return ModelResponce getObject = Array UserContract
     * @param mixed $filter Array UserFilter
     */
    public static function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();// UserContract's
        
        // Формирование SQL запроса
        $sql = "select * from `evt_user` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);
        
        // Выполнение SQL запроса
        $ar = UserActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new UserContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{UserContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{UserContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($ArrayCriteria, $sql, $tag) {
        $parameter = $ArrayCriteria;
        if($parameter instanceof UserFilter) {
            str_replace("'", "%", $parameter->KeyValue);
            str_replace('"', "%", $parameter->KeyValue);
            switch($parameter->KeyType) {
                case UserFilter::EMAIL:
                    $sql = $sql."`email` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::FIRST_NAME:
                    $sql = $sql."`first_name` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::LAST_NAME:
                    $sql = $sql."`last_name` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::MIDLE_NAME:
                    $sql = $sql."`midle_name` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::PHONE:
                    $sql = $sql."`phone` like '".$parameter->KeyValue."' ".$tag;
                    break;
                
                case UserFilter::ID:
                    $sql = $sql."`id`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::BIRTHDAY:
                    $sql = $sql."`birthday` like'".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::GENDER:
                    $sql = $sql."`id_gender`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::ID_MEDIA:
                    $sql = $sql."`id_media`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::ID_STATUS:
                    $sql = $sql."`id_status`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::ID_USER_ROLE:
                    $sql = $sql."`id_user_role`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::IS_MODERATED:
                    $sql = $sql."`id_moderated`=".$parameter->KeyValue." ".$tag;
                    break;
                case UserFilter::PASSWORD:
                    $sql = $sql."`password` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::PHONE:
                    $sql = $sql."`phone` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::SITE:
                    $sql = $sql."`site` like '".$parameter->KeyValue."' ".$tag;
                    break;
                case UserFilter::SKYPE:
                    $sql = $sql."`skype` like '".$parameter->KeyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }
    
    public function assign($obj) {
        if ($obj instanceof UserActiveRecord) {
            $this->ThisEvtUser = $obj;
        }
    }
    public function delete() {
        $this->ThisEvtUser->delete();
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
}