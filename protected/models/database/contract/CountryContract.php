<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class CountryContract extends DBObject {
    private $_countryActiveRecord;

    public function getId() {
        return $this->_countryActiveRecord->id;
    }
    public function getName() {
        return $this->_countryActiveRecord->name;
    }
    public function setName($value) {
        $this->_countryActiveRecord->name = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::COUNTRY;
        $this->_countryActiveRecord = new CountryActiveRecord();
    }

    public function create() {
        $this->_countryActiveRecord->model()->save();
    }
    public function delete() {
        $this->_countryActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array CountryContract
     * @param mixed $filter Array CountryFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_country` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = CountryActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new CountryContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{CountryContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{CountryContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof CountryFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case CountryFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case CountryFilter::NAME:
                    $sql = $sql."`name` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }
    public function update() {
        $this->_countryActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_countryActiveRecord = $obj;
    }
}
