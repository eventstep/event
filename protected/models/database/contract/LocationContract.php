<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CityContract
 *
 * @author snxGalaxy
 */
class LocationContract extends DBObject {
    private $_locationActiveRecord;
    
    public function getId() {
        return $this->_locationActiveRecord->Id;
    }
    
    public function getCoordinate() {
        $coordinateFilter = new CoordinateFilter();
        $coordinateFilter->keyType = CoordinateFilter::ID;
        $coordinateFilter->keyValue = $this->_locationActiveRecord->id_coordinate;
        
        return CoordinateContract::find($coordinateFilter)->getObject();
    }
    
    public function setCoordinate($value) {
        $this->_locationActiveRecord->id_coordinate = $value->getId();
    }
    
    public function getCity() {
        $cityFilter = new CityFilter();
        $cityFilter->keyType = CityFilter::ID;
        $cityFilter->keyValue = $this->_locationActiveRecord->id_city;
        
        return CityContract::find($cityFilter)->getObject();
    }
    
    public function setCity($value) {
        $this->_locationActiveRecord->id_city = $value->getId();
    }
    
    public function getName() {
        return $this->_locationActiveRecord->name;
    }
    
    public function setName($value) {
        $this->_locationActiveRecord->name = $value;
    }
    
    public function getAddress() {
        return $this->_locationActiveRecord->address;
    }
    
    public function setAddress($value) {
        $this->_locationActiveRecord->address = $value;
    }
    
    public function __construct() {
        $this->contentType = ContentTypeContract::LOCATION;
        $this->_locationActiveRecord = new LocationActiveRecord();
    }
    
    public function create() {
        $this->_locationActiveRecord->model()->save();
    }

    public function delete() {
        $this->_locationActiveRecord->model()->delete();
    }

    /**
     * @return ModelResponce getObject = Array LocationContract
     * @param mixed $ArrayCriteria Array LocationFilter
     */
    public function find($ArrayCriteria) {
        $rezult = new ModelResponse();
        $ctr_ar = array();
        
        // Формирование SQL запроса
        $sql = "select * from `evt_location` where ";
        if (is_array($ArrayCriteria)) {
            $Select = Filter::LINQ_SelectFromArray($ArrayCriteria);
            foreach($Select as $I => $arrayOR) {
                if ($arrayOR == null) continue;
                if(count($arrayOR) >= 2) {
                    $sql = $sql." (";
                    foreach($arrayOR as $J => $parameter) {
                        $sql = formatSql($parameter, $sql, "or");
                    }
                    $sql = substr($sql, 0, -3);// remove ' or'
                    $sql = $sql." ) and";
                } else {
                    $sql = formatSql($arrayOR[0], $sql, "and");
                }
            }
        } else {
            $sql = formatSql($ArrayCriteria, $sql, "and");
        }
        $sql = substr($sql, 0, -4);// remove ' and'
        
        // Выполнение SQL запроса
        $ar = LocationActiveRecord::model()->findAllBySql($sql);// ищем всех юзеров по заданым критериям
        $z = 0;
        
        // Conver ActiveRecord To Contract Class
        foreach ($ar as $item) {
            $ctr_ar[$z] = new LocationContract();
            $ctr_ar[$z]->assign($item);
            $z++;
        }
        
        // Set Rezult
        if($z >= 1) {
            $rezult->setMessage("{LocationContract::find}[Length = ".$z."]");
            $rezult->setIsCorrect(TRUE);
        } else {
            $rezult->setMessage("{UserContract::find}[Length = 0]");
            $rezult->setIsCorrect(FALSE);
        }
        $rezult->setObject($ctr_ar);
        return $rezult;
    }
    private static function formatSql($criteria, $sql, $tag) {
        if($criteria instanceof LocationFilter) {
            str_replace("'", "%", $criteria->KeyValue);
            str_replace('"', "%", $criteria->KeyValue);
            switch ($criteria->keyType) {
                case LocationFilter::ID:
                    $sql = $sql."`id`=".$criteria->keyValue." ".$tag;
                    break;
                case LocationFilter::ADDRESS:
                    $sql = $sql."`adress` like '".$criteria->keyValue."' ".$tag;
                    break;
                case LocationFilter::ID_CITY:
                    $sql = $sql."`id_city`=".$criteria->keyValue." ".$tag;
                    break;
                case LocationFilter::ID_COORDINATE:
                    $sql = $sql."`id_coordinate`=".$criteria->keyValue." ".$tag;
                    break;
                case LocationFilter::NAME:
                    $sql = $sql."`name` like '".$criteria->keyValue."' ".$tag;
                    break;
            }
        }
        return $sql;
    }

    public function read($filterFrom, $count = 0) {
        /// TODO : Relize Method
    }

    public function update() {
        $this->_locationActiveRecord->model()->update();
    }
    public function assign($obj) {
        $this->_locationActiveRecord = $obj;
    }
}
