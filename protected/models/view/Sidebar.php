<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 28, 2014
 * Description: 
 */

/**
 * Description of Sidebar
 *
 * @author student
 */
class Sidebar extends ViewPart {
    public $blockFilter;
    public $blockAdvertisement;
    
    public function __construct() {
        parent::__construct();
        
        $this->blockFilter = new BlockFilter();
        $this->blockAdvertisement = new BlockAdvertisement();
    }
}
