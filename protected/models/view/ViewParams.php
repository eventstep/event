<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 28, 2014
 * Description: 
 */

/**
 * Description of ViewParams
 *
 * @author student
 */
class ViewParams {
    public $header;
    public $sidebar;
    
    public function __construct() {
        $this->header = new Header();
        $this->sidebar = new Sidebar();
    }
}
