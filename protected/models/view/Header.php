<?php

/*
 * Author: Siderka Eugene
 * Date: Dec 1, 2014
 * Description:
 */

/**
 * Description of Header
 *
 * @author snxGalaxy
 */
class Header extends ViewPart {
    public $blockAuthorization;
    
    public function __construct() {
        parent::__construct();
        
        $this->blockAuthorization = new BlockAuthorization();
    }
}
