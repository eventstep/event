<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of ModelResponse
 *
 * @author snxGalaxy
 */
class ModelResponse {
    /**
     *
     * @var boolean
     */
    private $_isCorrect;
    
    public function getIsCorrect() {
        return $this->_isCorrect;
    }
    
    public function setIsCorrect($value) {
        if (is_bool($value)) {
            $this->_isCorrect = $value;
        }
    }

    /**
     *
     * @var string[]
     */
    private $_message;
    
    public function getMessage() {
        return $this->_message;
    }
    
    public function setMessage($value) {
        $this->_message[] = $value;
    }
    
    /**
     *
     * @var object
     */
    private $_object;
    
    public function getObject() {
        return $this->_object;
    }
    
    public function setObject($value) {
        $this->_object = $value;
    }
    
    public function __construct() {
        $this->_isCorrect = FALSE;
        $this->_message = array();
    }
    
    public function copyMessages($source) {
        foreach ($source->getMessage() as $m) {
            $this->setMessage($m);
        }
    }
    
    public function showMessages() {
        foreach ($this->getMessage() as $m) {
            echo sprintf('%s</ br>', $m);
        }
    }
}
