<?php

/*
 * Author: Siderka Eugene
 * Date: Dec 1, 2014
 * Description: 
 */

/**
 * Description of LayoutBehavior
 *
 * @author student
 */
class LayoutBehavior extends CBehavior {
    public $team = 'ITStep-9PV2';
    public $viewParams;
    
    public function __construct() {
        $this->viewParams = new ViewParams();
    }
}
