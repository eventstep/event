<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of CommentFormAdd
 *
 * @author snxGalaxy
 */
class CommentFormAdd extends CFormModel {
    public $contentType;
    public $contentKey;
    public $idUser;
    public $text;
    public $textAlias;
    public $datetime;
    
    public function __construct($scenario = '') {
        parent::__construct($scenario);
        
        $this->textAlias = 'Комментарий';
    }
    
    public function rules() {
        return array(
            array('text', 'required', 'message' => 'Поле '.$this->textAlias.' не может быть пустым')
        );
    }
}
