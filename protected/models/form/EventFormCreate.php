<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of EventFormCreate
 *
 * @author snxGalaxy
 */
class EventFormCreate extends CFormModel {
    public $title;
    public $titleAlias;
    public $description;
    public $descriptionAlias;
    public $price;
    public $priceAlias;
    public $locationLatitude;
    public $locationLongitude;
    public $locationCity;
    public $locationCityAlias;
    public $locationCityPattern;
    public $locationCityCountry;
    public $locationCityCountryAlias;
    public $locationCityCountryPattern;
    public $locationAddress;
    public $locationAddressAlias;
    public $locationName;
    public $locationNameAlias;
    public $eventTypeName;
    public $eventTypeNameAlias;
    public $eventTypeNamePattern;
    public $capacity;
    public $capacityAlias;
    public $dateStart;
    public $dateStartAlias;
    public $keyword;
    public $keywordAlias;
    
    public function __construct($scenario = '') {
        parent::__construct($scenario);
        
        $this->titleAlias = 'Название';
        $this->descriptionAlias = 'Описание';
        $this->priceAlias = 'Стоимость';
        $this->locationCityAlias = 'Город';
        $this->locationCityCountryAlias = 'Страна';
        $this->locationAddressAlias = 'Адресс';
        $this->locationNameAlias = 'Организация/Заведение';
        $this->eventTypeNameAlias = 'Тип мероприятия';
        $this->capacityAlias = 'Количество мест';
        $this->dateStartAlias = 'Дата начала';
        $this->keywordAlias = 'Ключевые слова';
        
        $this->locationCityPattern = '/^[А-Яа-я]+$/u';
        $this->locationCityCountryPattern = '/^[А-Яа-я]+$/u';
        $this->eventTypeNamePattern = '/^[А-Яа-я]+$/u';
    }
    
    public function rules() {
        return array(
            array(
                'title', 
                'required', 
                'message' => 'Поле "'.$this->titleAlias.'" не может быть пустым'
            ),
            array(
                'description', 
                'required', 
                'message' => 'Поле "'.$this->descriptionAlias.'" не может быть пустым'
            ),
            array(
                'price', 
                'numeric', 
                'message' => 'Поле "'.$this->price.'" должно быть числовым'
            ),
            array(
                'locationCity', 
                'required', 
                'message' => 'Поле "'.$this->locationCityAlias.'" не может быть пустым'
            ),
            array(
                'locationCity', 
                'match', 
                'pattern' => $this->locationCityPattern, 
                'message' => 'Поле "'.$this->locationCityAlias.'" введено некорректно'
            ),
            array(
                'locationCountry', 
                'required', 
                'message' => 'Поле "'.$this->locationCityCountryAlias.'" не может быть пустым'
            ),
            array(
                'locationCountry', 
                'match', 
                'pattern' => $this->locationCityCountryPattern, 
                'message' => 'Поле "'.$this->locationCityCountryAlias.'" введено некорректно'
            ),
            array(
                'locationAddress', 
                'required', 
                'message' => 'Поле "'.$this->locationAddressAlias.'" не может быть пустым'
            ),
            array(
                'eventTypeName', 
                'match', 
                'pattern' => $this->eventTypeNamePattern, 
                'message' => 'Поле "'.$this->eventTypeNameAlias.'" введено некорректно'
            ),
            array(
                'dateStart', 
                'required', 
                'message' => 'Поле "'.$this->dateStartAlias.'" не может быть пустым'
            )
        );
    }
    
}
