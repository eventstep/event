<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestForm
 *
 * @author student
 */
class TestForm extends CFormModel {
    public $username;
    public $password;
    public $rememberMe = false;
    
    private $_identity;
    
    public function rules() {
        return array(
            array('username, password', 'required'),
            array('rememberMe', 'boolean'),
            array('password', 'authenticate')
        );
    }
    
    public function authenticate($attribute, $params) {
        $this->_identity = new UserIdentity($this->username, $this->password);
        
        if (!$this->_identity->authenticate()) {
            $this->addError('password', 'Wrong username or password');
        }
    }
}
