<?php

/*
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description:
 */

/**
 * Description of EventFormCreate
 *
 * @author snxGalaxy
 */
class EventFormDetails {
    public $title;
    public $titleAlias;
    public $mark;
    public $markAlias;
    public $comment;
    public $commentAlias;
    public $media;
    public $mediaAlias;
    public $subscriber;
    public $subscriberAlias;
    public $member;
    public $memberAlias;
    public $leader;
    public $leaderAlias;
    public $sponsor;
    public $sponsorAlias;
    
    public function __construct($scenario = '') {
        parent::__construct($scenario);
        
        
    }
}
