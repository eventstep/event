<?php

/* 
 * Author: Siderka Eugene
 * Date: Nov 24, 2014
 * Description: 
 */

return array(
    'name' => 'Eventer',
    'language' => 'ru',
    'defaultController' => 'home',
    
    'aliases' => array(
        'bootstrap' => realpath(__DIR__.'/../extensions/bootstrap')
    ),
    
    'import' => array(
        'application.models.*',
        'application.models.view.*',
        'bootstrap.helpers.*'
    ),
    
    'behaviors' => array(
        'layout' => 'LayoutBehavior'
    ),
    
    'modules' => array(
        'gii' => array(
            'generatorPaths' => array(
                'bootstrap.gii'
            )
        )
    ),
    
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=v80.mysql.ukraine.com.ua;dbname=v80_eventer',
            'username' => 'v80_eventer',
            'password' => 'sfdswx4n',
            'charset' => 'utf8'
        ),
        
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi'
        )
    )
);