<?php

/* 
 * Author: Siderka Eugene
 * Date: Nov 28, 2014
 * Description: 
 */

?>

<div id="filter">
    <table>
        <tr>
            <?php echo CHtml::button('Maps',array('id' => 'Map'));?>

            <?php echo CHtml::ajaxButton(
                    'Update', 
                    CController::createUrl('home/updateContent'), 
                    array(
                        'update' => '#myUpdateArea'
                    )
            ); ?>
        </tr>
        <tr>
            <?php echo CHtml::button('Search',array('id' => 'Search'));?>
        </tr>
        <tr>
            <?php echo CHtml::button('View',array('id' => 'View'));?>
        </tr>
    </table>
    <div id="reklama">
        <h3>Тут могла быть ваша реклама</h3>
    </div>
</div>
