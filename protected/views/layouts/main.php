<!DOCTYPE html>
<!--test-->
<html>
    <head>
        <?php Yii::app()->bootstrap->register(); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/main.css'); ?>
        
        <title>
            <?php echo CHtml::encode($this->pageTitle); ?>
        </title>
    </head>
    <body>
        <div class="container">
            <div class="row header">
                <div class="span2">
                    <img src="http://placehold.it/200x100" />
                </div>
                <div class="span7">
                    <h1><?php echo Yii::app()->name; ?></h1>
                </div>
                <div class="span3">
                    <?php if (Yii::app()->viewParams->header->blockAuthorization->isShow) include 'blockAuthorization.php'; ?>
                </div>
            </div>
            <div class="row content">
                <div class="span9">
                    <?php echo $content; ?>
                </div>
                <div class="span3">
                    <?php if (Yii::app()->viewParams->sidebar->blockFilter->isShow) include 'blockFilter.php'; ?>
                    <?php if (Yii::app()->viewParams->sidebar->blockAdvertisement->isShow) include 'blockAdvertisement.php'; ?>
                </div>
            </div>
            <div class="row footer">
                <div class="span4">
                    <p>Menu</p>
                </div>
                <div class="span4">
                    <?php echo Yii::app()->name; ?>
                    &copy;
                    <?php echo date('Y'); ?>
                </div>
                <div class="span4">
                    <?php echo Yii::app()->team; ?>
                </div>
            </div>
        </div>
    </body>
</html>