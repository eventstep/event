<?php

/* 
 * Author: Siderka Eugene
 * Date: Dec 1, 2014
 * Description:
 */

?>

<div class="block authorization">
    <div class="row-fluid">
        <?php echo TbHtml::textField('txf', '', array(
            'prepend' => 'Login',
            'size' => TbHtml::INPUT_SIZE_MEDIUM
        )) ?>
    </div>
    <div class="row-fluid">
        <?php echo TbHtml::textField('txf', '', array(
            'prepend' => 'Password',
            'size' => TbHtml::INPUT_SIZE_MEDIUM
        )) ?>
    </div>
    <div class="row-fluid">
        <?php echo CHtml::submitButton('Sing in',array('id' => 'SingIn'));?>
        <?php echo CHtml::submitButton('Registration',array('id' => 'Reg'));?>
    </div>
</div>
